Para la realizacion de la api se ha utilizado Lumen (microframework de Laravel).
La resolución del problema se ha planteado mediante la utilización de REDIS (base de datos en memoria), previa carga de la información en una base de datos mysql.
Gracias a esto podemos obtener tiempos de respuestas muchos mejores que los planteados en el enunciado (podemos obtener la información en menos de 30 ms).
Para poder realizar el llenado de esta base de datos cache, he desarrollado un comando llamado load-zip-codes-into-cache que toma la información de mysql, y llena la cache.
El mismo puede ser ejecutado de la siguiente forma

php artisan load-zip-codes-into-cache

El procedimiento que se hizo es el siguiente:

1 - Se creo un seeder llamado ZipCodesSeeder que realiza el proceso de carga en la base de datos mysql. El mismo, realiza la lectura del xls que se encuentra en storage/app (para esto se usa la librería de PhpSpreadsheet) y guarda en bd mysql los registros.

2 - Una vez que tenemos los registros en la base de datos mysql, procedemos a ejecutar el comando

php artisan load-zip-codes-into-cache

Este comando básicamente lo que hace es se agrupar las filas por d_codigo.  Entonces lo que hago es armar la estructura json de cada código y dentro de ella agregamos los asentamientos, municipalidad y demás datos.
Como la base de datos redis funciona con la estructura (clave,valor) la clave es el d_codigo y el valor es el json con toda la información ya procesada y con el formato necesario.

3- Una vez cargada la base de datos en memoria, solo hay que consultarla por codigo y no hace falta procesar nada (ya que lo hicimos en el paso anterior y evitamos esa demora durante la consulta).

Con esto logramos que si el día de mañana se quieren actualizar la información de algún asentamiento, localidad o lo que fuera, simplemente actualizamos el registro de la base de datos mysql y actualizamos sólo ese valor en la cache.

4 - Además de esto, se ha realizado un test sencillo para comparar la devolución de esta api desarrollada, con la api de

https://jobs.backbonesystems.io/api/zip-codes/

4.1 - Mediante la ejecución del mismo (por defecto se prueban 100 códigos distintos aleatorios, pero ustedes pueden cambiar este número agregando una variable en el env con el nombre CANT_TEST), he detectado algunos problemas que se deben tener en cuenta a la hora de evaluar este proyecto.

4.1.1 - En la API original de backbonesystems, todas las letras "ñ" se devuelven como "?", por lo cual no es lo mismo

cañada != ca?ada.

Por lo cual he utilizado una función durante el llenado de la base de datos redis, para poner todas las 'ñ' como '?' y minimizar los errores de comparación.
Además se quitan todos los acentos del xls (ya que la api de backbonesystems devuelve sin acentos) y se ponen los registros necesarios en mayúsculas.

4.1.2 - Se han visto distintas observacion que pueden provocar que el resultado devuelto por backbonesystems sea distinto al desarrollado en este proyecto. Por ejemplo

a - Para el código 79317
En la api de backbonesystems RANCHO EL CIMARRON viene en este código, pero en el archivo xls no está, por lo cual en la api desarrollada por mí no aparece.

b - Para el codigo 23593
En la api de backbonesystems vienen solo "EL SAUZAL" y  "BOCA DEL SAUCITO", pero en el xls de descarga veo que hay uno mas que se llama "LOS POZOS"

El caso a y b demuestra que no es la misma cantidad de registro que hay en la api de backbonesystems con el xls que se descarga de la página que dice el enunciado

c - Para los códigos 30500 y 50100
Devuelven los array en distinto orden por lo cual al ejecutar un test de assertEquals, falla.


Estos son algunos ejemplos nomás de las diferencias que se pueden encontrar entre ambas apis. En caso de que se requiera un cambio y se deba cargar otro xls, se ejecutan sin problema los comandos.

Para ejecutar el test basta con ejecutar el siguiente comando

vendor/bin/phpunit --filter=ZipCodesTest

ACLARACIÓN
Se decidió utilizar la base de datos en memoria y no base de datos MYSQL, ya que se entendió que lo principal del enunciado era el formato y tiempo de respuesta de la api.
Usando la cache, obtenemos un mejor rendimiento aproximadamente del 100%.

Para poder comprobar esto, se ha publicado otra API

http://179.43.126.244:8080/api/zip-codes-by-sql/20010

que lo que hace es devolver la información consultando en la base de datos mysql en lugar de la cache. Con eso podemos comprobar que el rendimiento es casi un 100% más rápido si lo hacemos desde la cache.


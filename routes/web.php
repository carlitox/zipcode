<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/*$router->get('/', function () use ($router) {
    return $router->app->version();
});*/

$router->group([
    'prefix' => 'api',
], function () use ($router){
    $router->get('/zip-codes/', function (){
        return "Ingrese un código válido con el formato ".env('APP_URL').'/api/zip-codes/{zip_code}';
    });

    $router->get('/zip-codes/{zip_code}', 'ZipCodeController@search');
    $router->get('/zip-codes-by-sql/{zip_code}', 'ZipCodeController@search_by_sql');
});

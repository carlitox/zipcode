<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Redis;
use Storage;
use App\Models\ZipCode;

class ZipCodesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        dump(date('d-m-Y H:i:s'));

        /**clear cache radius**/
        //Redis::flushDB();

        /**lOAD file*/
        $path = storage_path() . '/app/zip-codes.xls';
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
        $spreadsheet = $reader->load($path);

        /**declare var to read file**/
        $head = [
            'd_codigo','d_asenta','d_tipo_asenta','D_mnpio','d_estado','d_ciudad','d_CP',
            'c_estado','c_oficina','c_CP','c_tipo_asenta','c_mnpio','id_asenta_cpcons','d_zona','c_cve_ciudad'
        ];
        $position_d_codigo = array_search('d_codigo',$head);
        $position_d_asenta = array_search('d_asenta',$head);
        $position_d_tipo_asenta = array_search('d_tipo_asenta',$head);
        $position_D_mnpio = array_search('D_mnpio',$head);
        $position_d_estado = array_search('d_estado',$head);
        $position_d_ciudad = array_search('d_ciudad',$head);
        $position_d_CP = array_search('d_CP',$head);
        $position_c_estado = array_search('c_estado',$head);
        $position_c_oficina = array_search('c_oficina',$head);
        $position_c_CP = array_search('c_CP',$head);
        $position_c_tipo_asenta = array_search('c_tipo_asenta',$head);
        $position_c_mnpio= array_search('c_mnpio',$head);
        $position_id_asenta_cpcons = array_search('id_asenta_cpcons',$head);
        $position_d_zona = array_search('d_zona',$head);
        $position_c_cve_ciudad = array_search('c_cve_ciudad',$head);

        /**Read all sheet**/
        $document = $spreadsheet->getAllSheets();
        /**quit first sheet*/
        unset($document[0]);
        /**create array general**/
        $array_zip_codes = array();

        /**iterate for all sheet**/
        foreach($document as $sheet){

            $reader = $sheet->toArray();
            /**quit head of sheet*/
            unset($reader[0]);

            foreach ($reader as $row) {
                if(is_null($row[$position_d_codigo]))
                    continue;
               /**create element database*/
                $zip_code_row = [
                    'd_codigo' => $row[$position_d_codigo],
                    'd_asenta' => $row[$position_d_asenta],
                    'd_tipo_asenta' => $row[$position_d_tipo_asenta],
                    'D_mnpio' => $row[$position_D_mnpio],
                    'd_estado'=> $row[$position_d_estado],
                    'd_ciudad'=> $row[$position_d_ciudad],
                    'd_CP'=> $row[$position_d_CP],
                    'c_estado' => $row[$position_c_estado],
                    'c_oficina' => $row[$position_c_oficina],
                    'c_CP' => $row[$position_c_CP],
                    'c_tipo_asenta' => $row[$position_c_tipo_asenta],
                    'c_mnpio' => $row[$position_c_mnpio],
                    'id_asenta_cpcons' => $row[$position_id_asenta_cpcons],
                    'd_zona' => $row[$position_d_zona],
                    'c_cve_ciudad' => $row[$position_c_cve_ciudad]
                ];
                /**add into array zip codes to save all together**/
                array_push($array_zip_codes,$zip_code_row);
            }
        }
        /***save all*/
        ZipCode::insert($array_zip_codes);
        dump(date('d-m-Y H:i:s'));

    }

}

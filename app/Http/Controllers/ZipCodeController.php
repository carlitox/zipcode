<?php

namespace App\Http\Controllers;

use App\Models\ZipCode;
use Illuminate\Support\Facades\Redis;
use Storage;
use stdClass;


class ZipCodeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function search($zip_code = null)
    {
        $elemento = Redis::get($zip_code);
        return response()->json(json_decode($elemento));
    }


    /**BACKUP method**/
    public function search_by_sql($zip_code = null)
    {

        $zip_codes = ZipCode::where('d_codigo',$zip_code)->get();
        $group_by_code = $zip_codes->groupBy('d_codigo');

        $object_zip_code = [];

        foreach ($group_by_code as $settlements) {

            /**general Object**/
            $object_zip_code = new stdClass;
            $object_zip_code->zip_code = $settlements->first()->d_codigo;
            $object_zip_code->locality = strtoupper($this->remove_accents($settlements->first()->d_ciudad));

            /**federal entity Object**/
            $object_federal_entity = new stdClass;
            $object_federal_entity->key = $settlements->first()->c_estado;
            $object_federal_entity->name = strtoupper($this->remove_accents($settlements->first()->d_estado));
            $object_federal_entity->code = null; // TODO revisar
            $object_zip_code->federal_entity = $object_federal_entity;

            /**settlements Objects**/
            $list_settlements = array();
            foreach($settlements as $detail){
                $object_settlements = new stdClass;
                $object_settlements->key = $detail->id_asenta_cpcons;
                $object_settlements->name = strtoupper($this->remove_accents($detail->d_asenta));
                $object_settlements->zone_type = strtoupper($this->remove_accents($detail->d_zona));

                $object_settlements_type = new stdClass;
                $object_settlements_type->name = $detail->d_tipo_asenta;
                $object_settlements->settlement_type = $object_settlements_type;
                array_push($list_settlements,$object_settlements);
            }
            $object_zip_code->settlements = $list_settlements;

            /**municipality Object**/
            $object_municipality = new stdClass;
            $object_municipality->key = intval($settlements->first()->c_mnpio);
            $object_municipality->name = strtoupper($this->remove_accents($detail->D_mnpio));
            $object_zip_code->municipality = $object_municipality;

        }

        return response()->json($object_zip_code);

    }

    function remove_accents($chain){
        /*$originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿ';
        $modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyyby';*/

        $original = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿ';
        $modify = 'aaaaaaaceeeeiiiid?oooooouuuuybsaaaaaaaceeeeiiiid?oooooouuuyyby';


        $chain = utf8_decode($chain);
        $chain = strtr($chain, utf8_decode($original), $modify);
        return utf8_encode($chain);
    }

}

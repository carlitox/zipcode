<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;
use Illuminate\Database\Seeder;
use Storage;
use stdClass;
use App\Models\ZipCode;

class LoadZipCodesIntoCache extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'load-zip-codes-into-cache';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get data of database and load information into Redis cache';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        dump(date('d-m-Y H:i:s'));

        /**clear cache radius**/
        //Redis::flushDB();

        $zip_codes = ZipCode::all();
        $group_by_code = $zip_codes->groupBy('d_codigo');

        foreach ($group_by_code as $settlements) {

            /**general Object**/
            $object_zip_code = new stdClass;
            $object_zip_code->zip_code = $settlements->first()->d_codigo;
            $object_zip_code->locality = strtoupper($this->remove_accents($settlements->first()->d_ciudad));

            /**federal entity Object**/
            $object_federal_entity = new stdClass;
            $object_federal_entity->key = $settlements->first()->c_estado;
            $object_federal_entity->name = strtoupper($this->remove_accents($settlements->first()->d_estado));
            $object_federal_entity->code = null; // TODO revisar
            $object_zip_code->federal_entity = $object_federal_entity;

            /**settlements Objects**/
            $list_settlements = array();
            foreach($settlements as $detail){
                $object_settlements = new stdClass;
                $object_settlements->key = $detail->id_asenta_cpcons;
                $object_settlements->name = strtoupper($this->remove_accents($detail->d_asenta));
                $object_settlements->zone_type = strtoupper($this->remove_accents($detail->d_zona));

                $object_settlements_type = new stdClass;
                $object_settlements_type->name = $detail->d_tipo_asenta;
                $object_settlements->settlement_type = $object_settlements_type;
                array_push($list_settlements,$object_settlements);
            }
            $object_zip_code->settlements = $list_settlements;

            /**municipality Object**/
            $object_municipality = new stdClass;
            $object_municipality->key = intval($settlements->first()->c_mnpio);
            $object_municipality->name = strtoupper($this->remove_accents($detail->D_mnpio));
            $object_zip_code->municipality = $object_municipality;

            Redis::set($settlements->first()->d_codigo, json_encode($object_zip_code));

        }

        dump(date('d-m-Y H:i:s'));

    }

    function remove_accents($chain){
        /*$originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿ';
        $modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyyby';*/

        $original = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿ';
        $modify = 'aaaaaaaceeeeiiiid?oooooouuuuybsaaaaaaaceeeeiiiid?oooooouuuyyby';


        $chain = utf8_decode($chain);
        $chain = strtr($chain, utf8_decode($original), $modify);
        return utf8_encode($chain);
    }
}

<?php

namespace Tests;

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Redis;
use Illuminate\Http\Request;
use Log;

class ZipCodesTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_zip_codes()
    {

        /**take 100 elements aleatory to test*/
        $all = Redis::keys('*');
        $random_keys=array_rand($all,env('CANT_TEST',100));
        $client = new Client();

        foreach ($random_keys as $code){

                Log::info('testing :'.$all[$code]);

                $json_local = $client->request('GET',env('APP_URL').'/api/zip-codes/'.$all[$code]);
                $status = $json_local->getStatusCode();
                if($status == 200){
                    $json_local = json_decode($json_local->getBody()->getContents());
                }

                $json_remote = $client->request('GET', 'https://jobs.backbonesystems.io/api/zip-codes/'.$all[$code]);
                $status = $json_remote->getStatusCode();

                if($status == 200){
                    $json_remote = json_decode($json_remote->getBody()->getContents());
                    $this->assertEquals($json_local,$json_remote,'testing :'.$all[$code]);
                }

        }
    }
}
